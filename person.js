class Person {
  _name;

  // Construtor
  constructor(name) {
    this._name = name;
  }
  // Construtor

  // Getters
  getName() {
    return `My name is ${this._name}`;
  }
  // Getters
}

module.exports = { Person };
