const path = require("path");

// Apenas o nome do arquivo atual
console.log(path.basename(__filename));
// Apenas o nome do arquivo atual

// Nome do diretório atual
console.log(path.dirname(__filename));
// Nome do diretório atual

// Extensão do arquivo
console.log(path.extname(__filename));
// Extensão do arquivo

// Criar objeto Path
console.log(path.parse(__filename));
// Criar objeto Path

// Juntar caminhos de arquivos
console.log(path.join(__dirname, "test", "test.html"));
// Juntar caminhos de arquivos
