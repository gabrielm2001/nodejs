const fs = require("fs");
const path = require("path");

// Criar uma pasta
// fs.mkdir(path.join(__dirname, "/test"), (error) => {
//   if (error) {
//     return console.log(error);
//   }

//   console.log("Pasta criada com sucesso");
// });
// Criar uma pasta

// criar um arquivo Substitue o arquivo inteiro se ele existir
fs.writeFile(
  path.join(__dirname, "/test", "test.txt"),
  "Hello from NodeJs!",
  (err) => {
    if (err) {
      return console.log(err);
    }

    console.log("Arquivo criado com sucesso");
  },

  
  // Adicionar a um arquivo existente
  fs.appendFile(path.join(__dirname, "/test", "test.txt"), "Texto adiconado", (err)=>{
    if (err){
      return console.log(err)
    }

    console.log("Conteúdo adicionado com sucesso")
    
    // Ler arquivo
    fs.readFile(path.join(__dirname, "/test", "test.txt"), "utf-8",(err, data)=>{
      if (err){ 
        return console.log(err)
      }
    
      console.log(data)
      
    })
    // Ler arquivo
  })
  // Adicionar a um arquivo existente
  );
  // criar um arquivo Substitue o arquivo inteiro se ele existir








