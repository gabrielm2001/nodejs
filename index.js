// import the express module
const express = require("express");
const app = express();
// import the express module

// import the Person class
const PersonModel = require("./person");
const Person = PersonModel.Person;
// import the Person class

// require("./modules/path");

let person1 = new Person("Saladin");

console.log(person1.getName());
